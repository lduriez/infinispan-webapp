package org.lduriez.jcache.infinispan.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.infinispan.cdi.embedded.ConfigureCache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.jboss.marshalling.core.JBossUserMarshaller;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.transaction.TransactionMode;

public class InfinispanCacheConfig {
	
	/**
	 * Produces city cache configuration
	 * @return city cache configuration
	 */
	@CityCache
	@ConfigureCache("city-cache")
	@Produces
	public Configuration cityCacheConfiguration() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.clustering().cacheMode(CacheMode.REPL_SYNC);
		configurationBuilder.transaction().transactionMode(TransactionMode.NON_TRANSACTIONAL);
		configurationBuilder.memory().size(1000);
		return configurationBuilder.build();
	}
	
	/**
     * Creates {@link org.infinispan.manager.EmbeddedCacheManager} with JMX enabled.
     *
     * @param defaultConfiguration default cache configuration
     * @return EmbeddedCacheManager Cache manager
     */
    @Produces
    @ApplicationScoped 
    public EmbeddedCacheManager defaultClusteredCacheManager(@CityCache Configuration defaultConfiguration) { 
		GlobalConfigurationBuilder globalConfigurationBuilder = GlobalConfigurationBuilder.defaultClusteredBuilder();
		globalConfigurationBuilder.cacheManagerName("test-infinispan-cache-manager");
		globalConfigurationBuilder.defaultCacheName("test-infinispan-cache");
		globalConfigurationBuilder.jmx().enable().domain("test-infinispan-domain");
		globalConfigurationBuilder.transport().clusterName("test-infinispan-local-cluster");
		globalConfigurationBuilder.serialization().marshaller(new JBossUserMarshaller()).whiteList()
	    .addRegexps("org.lduriez.jcache.infinispan.*");
      return new DefaultCacheManager(globalConfigurationBuilder.build(), defaultConfiguration);
   }

}
