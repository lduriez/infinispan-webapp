package org.lduriez.jcache.infinispan.config;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.stream.Stream;

import javax.cache.annotation.CacheInvocationParameter;
import javax.cache.annotation.CacheKeyGenerator;
import javax.cache.annotation.CacheKeyInvocationContext;
import javax.cache.annotation.GeneratedCacheKey;

import org.infinispan.jcache.annotation.DefaultCacheKey;

/**
 * Custom cache key generator example
 *
 */
public class CityDtoCacheKeyGenerator implements CacheKeyGenerator {

	@Override
	public GeneratedCacheKey generateCacheKey(
			CacheKeyInvocationContext<? extends Annotation> cacheKeyInvocationContext) {
		Stream<Object> methodIdentity = Stream.of(cacheKeyInvocationContext.getMethod());
	    Stream<Object> parameterValues = Arrays.stream(cacheKeyInvocationContext.getKeyParameters()).map(CacheInvocationParameter::getValue);
	    return new DefaultCacheKey(Stream.concat(methodIdentity, parameterValues).toArray());
	  
	}

}
