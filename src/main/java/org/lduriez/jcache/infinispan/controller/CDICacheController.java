package org.lduriez.jcache.infinispan.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.cache.annotation.CacheKey;
import javax.cache.annotation.CacheResult;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.infinispan.Cache;
import org.infinispan.CacheSet;
import org.lduriez.jcache.infinispan.config.CityCache;
import org.lduriez.jcache.model.CityDto;

/**
 * Controller for CDI cache testing
 *
 */
@Named("CDICacheController")
@RequestScoped
public class CDICacheController {

	@Inject
	@CityCache
	private Cache<CacheKey, List<CityDto>> cache;

	/**
	 * Print to stdout cache name
	 */
	public void echoCacheName() {
		System.out.println("Cache name = " + cache.getName());
	}

	/**
	 * Get city cache size
	 * 
	 * @return city cache size
	 */
	public int getCityCacheSize() {
		int size = 0;
		System.out.println("Cache name = " + cache.getName());
		CacheSet<Entry<CacheKey, List<CityDto>>> cacheSet = cache.entrySet();
		for (Entry<CacheKey, List<CityDto>> entry : cacheSet) {
			System.out.println(entry.getKey());
			size = entry.getValue().size();
			System.out.println(size);
		}
		return size;
	}

	/**
	 * Fetch cities from datastore
	 * 
	 * @return all cities, unsorted
	 */
	@CacheResult(cacheName = "city-cache")
	public List<CityDto> getCities() {
		System.out.println("Fetching cities...");
		List<CityDto> cities = new ArrayList<>();
		cities.add(new CityDto("Paris"));
		cities.add(new CityDto("Suresnes"));
		return cities;
	}
}
